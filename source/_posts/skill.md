---
title : self assessment
date  : 2019-06-13 
---

Nobody is around without flaw, to pursue perfection, we need to understand our weaknesses and strengths. Thus, this document is compiled as a way to self measure my skills.

# Programming Skills

1. C
1. C++
1. Python
1. Javascript

# Design Skills

+ PCB/ Electronics
+ Multimedia

# Production skills

+ Woodworking
+ Soldering
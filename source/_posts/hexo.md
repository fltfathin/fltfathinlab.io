---
title: Hexo is useful
tags:
- website
- hexo
date: 2019-06-10
---

This is the first real post i made using `hexo`, it is pretty useful albeit hard to learn at the beginning. Hexo is a blog engine based on nodejs. It generates static site from markdown files

But it's 2019, why use static site?

Static site can be used in various hosting at a little to no cost. just like the first/ backup of this website residing on gitlab static site namely fltfathin.gitlab.io.

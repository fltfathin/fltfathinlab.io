---
title : skill self assessment
---

Nobody is around without flaw, to pursue perfection, we need to
understand our weaknesses and strengths. Thus, this document is
compiled as a way to self measure my skills.

# Programming

+ C
+ C++
+ Python
+ Javascript

# Design

+ PCB/ Electronics
+ Multimedia

# Production skills

+ Woodworking
+ Soldering

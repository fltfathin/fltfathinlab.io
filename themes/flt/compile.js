const pug = require('pug')
const fs = require('fs')
const glob = require('glob')
const beautify = require("js-beautify").html
glob('_src/**/*.pug',(err,files)=>{
    for (const file of files) {
        let res = pug.renderFile(file)
        res = res.replace(/\>[\ ]*\</g,'>\n<')
        res = beautify(res,{ indent_size: 1})
        let fcore = file.replace(/.pug$/i,".ejs")
        fcore = fcore.replace(/^_src\//i,'layout/')
        fs.writeFileSync(fcore,res)
        console.log(`${file} > ${fcore}`);
    }
})